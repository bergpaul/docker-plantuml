# Image docker pour compiler des fichiers PUML

## Exemple de `.gitlab-ci.yml`
```yaml
plant_uml:
  # Tag important pour le runner gitlab de l'UTC
  tags: 
    - docker
  image: registry.gitlab.utc.fr/bergpaul/docker-plantuml:latest
  script:
    - java -jar /app/plantuml.jar mon_fichier.puml
  artifacts:
    name: "$CI_COMMIT_SHORT_SHA uml"
    paths:
      - mon_fichier.png
```